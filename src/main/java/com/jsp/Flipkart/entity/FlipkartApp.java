package com.jsp.Flipkart.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity 
@Table

public class FlipkartApp  implements Serializable{
	
	
	
	
	
	@Id
	@Column(name="id")
	private Long id;
	 
	@Column(name="name")
	private String name;
	
	
	@Column(name="established_year")
	private int establishedyear;
	
	@Column(name="brand")
	private String brand;
	
	@Column(name="origin_country")
	private String origincountry;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getEstablishedyear() {
		return establishedyear;
	}


	public void setEstablishedyear(int establishedyear) {
		this.establishedyear = establishedyear;
	}


	public String getBrand() {
		return brand;
	}


	public void setBrand(String brand) {
		this.brand = brand;
	}


	public String getOrigincountry() {
		return origincountry;
	}


	public void setOrigincountry(String origincountry) {
		this.origincountry = origincountry;
	}
	
	
	

}
