package com.jsp.Flipkart.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.jsp.Flipkart.FlipkartAppConstant.FlipkartAppConstant;
@Entity
@Table(name=FlipkartAppConstant.Company_info)
public class Company implements Serializable {
	@Id
	@GenericGenerator(name = "m_auto",strategy="increment")
	@GeneratedValue(generator="m_auto")
	
	@Column (name="id")
	private Long id;
	
	@Column (name="name")
	private String name;
	
	@Column (name="ceo_Name")
	private String ceoName;
	@Column (name="head_Quaters")
	private String headQuaters;
	
	@Column (name="total_Employee")
	private String totalEmployee;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCeoName() {
		return ceoName;
	}
	public void setCeoName(String ceoName) {
		this.ceoName = ceoName;
	}
	public String getHeadQuaters() {
		return headQuaters;
	}
	public void setHeadQuaters(String headQuaters) {
		this.headQuaters = headQuaters;
	}
	public String getTotalEmployee() {
		return totalEmployee;
	}
	public void setTotalEmployee(String totalEmployee) {
		this.totalEmployee = totalEmployee;
	}
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="c_id")
	
private  List<Department>departmentList;

	public List<Department> getDepartmentList() {
		return departmentList;
	}
	public void setDepartmentList(List<Department> departmentList) {
		this.departmentList = departmentList;
	}
	public Company()
	{
		
	}

}
