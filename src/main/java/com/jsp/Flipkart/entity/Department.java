package com.jsp.Flipkart.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.jsp.Flipkart.FlipkartAppConstant.FlipkartAppConstant;
@Entity
@Table(name=FlipkartAppConstant.Department_info)
public class Department implements Serializable {
	
	@Id
	@GenericGenerator(name = "m_auto",strategy="increment")
	@GeneratedValue(generator="m_auto")
	@Column(name="id")
	private Long id;
	@Column(name="manager_Name")
	private String managerName;
	
	@Column(name="name")
	private String name;
	
	@Column(name="number_Of_Employoee")
	private String numberOfEmployee;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNumberOfEmployee() {
		return numberOfEmployee;
	}
	public void setNumberOfEmployee(String numberOfEmployee) {
		this.numberOfEmployee = numberOfEmployee;
	}
	
	public Department()
	{
		
	}
	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + ", numberOfEmployee=" + numberOfEmployee + "]";
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	

}
