package com.jsp.Flipkart.repository;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.jsp.Flipkart.entity.Company;




public class AssociationImpl implements Association 
{
	
	public void saveCompanyDetails(Company company) 
	{
		
		try {
			Configuration cfg = new Configuration();
			cfg.configure();
			SessionFactory sessionFactory = cfg.buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.save(company);
			transaction.commit();
			
			
		} catch (HibernateException e) {
			
			
		}
			
	}
	}
	


