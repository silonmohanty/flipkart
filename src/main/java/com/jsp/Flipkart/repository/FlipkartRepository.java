package com.jsp.Flipkart.repository;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.jsp.Flipkart.entity.FlipkartApp;


public class FlipkartRepository {
	
	
	public void saveFlipkartDetails(FlipkartApp flipkartapp)
	{
		try {
			Configuration cfg=new Configuration();
			cfg.configure();
			SessionFactory sessionfactory=cfg.buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.save(flipkartapp);
			transaction.commit();		
		}
		catch(HibernateException e)
		{
			
		}
	}

}
